import requests
import json
import discord
from discord.ext import commands

with open("token.txt") as token:
	TOKEN = token.read()



bot = commands.Bot(command_prefix='/')

@bot.command()
async def mod(ctx, arg):
    response = ((requests.get(f'https://api.modrinth.com/v2/search?limit=1&query={arg}')).json())
    try:
        response = ((response["hits"])[0])
    except:
        embed=discord.Embed(description="No Results Found", color=0xa51d2d)
        await ctx.send(embed=embed)
    title, author, desc, slug, img = (response["title"]), (response["author"]), (response["description"]), (response["slug"]), (response["icon_url"])
    url = (f"https://modrinth.com/mod/{slug}")
    embed=discord.Embed(title=title, url=url, description=desc, color=0x57e389)
    embed.set_author(name=author)
    embed.set_thumbnail(url=img)
    await ctx.send(embed=embed)

@bot.command()
async def mango(message):
    await message.author.send(":mango:")




bot.run(TOKEN)
